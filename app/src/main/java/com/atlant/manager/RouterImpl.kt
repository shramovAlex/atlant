package com.atlant.manager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.atlant.R
import com.atlant.manager.common.Router
import com.atlant.ui.fragment.AuthFragment
import com.atlant.ui.fragment.ProfileFragment


/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
class RouterImpl : Router {

    private fun replaceFragment(
        activity: FragmentActivity?,
        fragment: Fragment,
        containerId: Int,
        tag: String
    ) {
        activity?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(containerId, fragment, tag)
            ?.commitAllowingStateLoss()
    }

    override fun transferToAuthFragment(activity: FragmentActivity?) {
        replaceFragment(activity, AuthFragment(), R.id.main_activity_container, AuthFragment.TAG)
    }

    override fun transferToAccountProfileFragment(activity: FragmentActivity?) {
        replaceFragment(activity, ProfileFragment(), R.id.main_activity_container, ProfileFragment.TAG )
    }
}