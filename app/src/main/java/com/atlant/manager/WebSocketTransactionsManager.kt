package com.atlant.manager

import android.util.Log
import com.atlant.data.model.Transaction
import com.atlant.domain.response.TransactionResponseWrapper
import com.atlant.utils.APP_TAG
import com.atlant.utils.TRANSACTION_TIME_FORMAT
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.*
import okio.ByteString
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

/**
 * @author Alexandr Shramov
 * @date 07.12.2019
 */
class WebSocketTransactionsManager(
    private val mHttpClient: OkHttpClient,
    private val mSocketRequest: Request,
    private val foregroundDispatcher: CoroutineDispatcher
) {
    private val job = Job()

    private var mWebSocket: WebSocket? = null
    private val foregroundCoroutineContext: CoroutineContext
        get() = job + foregroundDispatcher
    private val foregroundScope = CoroutineScope(foregroundCoroutineContext)

    var onMessageReceived: (Transaction) -> Unit = {}
    var onError: (String) -> Unit = {}

    fun init() {
        mWebSocket = mHttpClient.newWebSocket(mSocketRequest, object: WebSocketListener() {
            override fun onOpen(webSocket: WebSocket, response: Response) {
                Log.i(APP_TAG, "onMessage: $response")
                webSocket.send("{\"op\":\"unconfirmed_sub\"}")
            }

            override fun onMessage(webSocket: WebSocket, text: String) {
                Log.i(APP_TAG, "onMessage: $text")
                val responseWrapper = Gson().fromJson(
                    text,
                    TransactionResponseWrapper::class.java
                )
                val element = responseWrapper?.element
                if(element != null) {
                    val calendar = Calendar.getInstance()
                    calendar.timeInMillis = element.time * 1000
                    val formattedDate = try {
                        SimpleDateFormat(TRANSACTION_TIME_FORMAT, Locale.getDefault())
                            .format(calendar.time)
                    } catch (e: Exception) {
                        ""
                    }
                    foregroundScope.launch {
                        onMessageReceived(Transaction(
                            element.hash ?: "",
                            formattedDate))
                    }
                }
            }

            override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
                Log.i(APP_TAG, "onMessage: $bytes")
            }

            override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
                Log.i(APP_TAG, "onFailure: ${t.message}")
                foregroundScope.launch {
                    onError(t.message ?: "")
                }
                t.printStackTrace()
            }

            override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
                Log.i(APP_TAG, "onClosing: $reason")
            }

            override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                Log.i(APP_TAG, "onClosed: $reason")
            }
        })
    }

    fun disconnect() {
        mWebSocket?.send("{\"op\":\"unconfirmed_unsub\"}")
        mWebSocket?.cancel()
    }
}