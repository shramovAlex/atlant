package com.atlant.manager.common

import androidx.fragment.app.FragmentActivity

/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
interface Router {

    fun transferToAuthFragment(activity: FragmentActivity?)

    fun transferToAccountProfileFragment(activity: FragmentActivity?)

}