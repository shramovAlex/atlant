package com.atlant.manager

import com.atlant.data.model.JwtToken


/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
object TokenManager {

    var jwtToken: JwtToken? = null

    override fun toString(): String {
        return "TokenManager(mAccount=${this.jwtToken})"
    }

    fun isAuthorized() = jwtToken != null

    fun deleteToken() {
        jwtToken = null
    }

}