package com.atlant.manager

import android.content.SharedPreferences

/**
 * @author Alexandr Shramov
 * @date 05.12.2019
 */
class SharedPreferencesManager(private val sharedPreferences: SharedPreferences) {

    fun putAccountToken(token: String) {
        sharedPreferences.edit()
            .putString(SHARED_PREF_ACCOUNT_TOKEN, token)
            .apply()
    }

    fun clearAccountToken() {
        sharedPreferences.edit().remove(SHARED_PREF_ACCOUNT_TOKEN).apply()
    }

    fun getAccountToken(): String? {
        return sharedPreferences.getString(SHARED_PREF_ACCOUNT_TOKEN, null)
    }

    companion object {
        const val SHARED_PREF_FILE_NAME = "AtlantPreferences"

        const val SHARED_PREF_ACCOUNT_TOKEN = "SHARED_PREF_ACCOUNT_TOKEN"
    }
}