package com.atlant.domain.usecase

import com.atlant.domain.entity.ApiErrorHandle
import com.atlant.domain.params.LogoutParams
import com.atlant.domain.usecase.common.BaseUseCase
import com.atlant.manager.SharedPreferencesManager
import com.atlant.network.ApiService
import kotlinx.coroutines.CoroutineDispatcher

/**
 * @author Alexandr Shramov
 * @date 08.12.2019
 */
class LogoutUseCase(
    backgroundDispatcher: CoroutineDispatcher,
    foregroundDispatcher: CoroutineDispatcher,
    private val mApiService: ApiService,
    apiErrorHandle: ApiErrorHandle,
    private val mSharedPreferencesManager: SharedPreferencesManager
) : BaseUseCase<Unit, LogoutParams>(
    backgroundDispatcher,
    foregroundDispatcher,
    apiErrorHandle
) {
    override suspend fun run(params: LogoutParams) {
        mApiService.logout(params)
        mSharedPreferencesManager.clearAccountToken()
    }
}