package com.atlant.domain.usecase

import com.atlant.data.model.ProfileInfo
import com.atlant.domain.entity.ApiErrorHandle
import com.atlant.domain.repository.ProfileInfoRepository
import com.atlant.domain.usecase.common.BaseUseCase
import kotlinx.coroutines.CoroutineDispatcher

/**
 * @author Alexandr Shramov
 * @date 07.12.2019
 */
class GetProfileInfoUseCase(
    backgroundDispatcher: CoroutineDispatcher,
    foregroundDispatcher: CoroutineDispatcher,
    private val mProfileInfoRepository: ProfileInfoRepository,
    apiErrorHandle: ApiErrorHandle
) : BaseUseCase<ProfileInfo, Unit>(
    backgroundDispatcher,
    foregroundDispatcher,
    apiErrorHandle
) {

    override suspend fun run(params: Unit): ProfileInfo {
        return mProfileInfoRepository.getProfileInfo()
    }
}