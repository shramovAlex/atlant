package com.atlant.domain.usecase.common


import com.atlant.data.model.ErrorModel
import com.atlant.domain.entity.ApiErrorHandle
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
abstract class BaseUseCase<Type, in Params>(
    private val backgroundDispatcher: CoroutineDispatcher,
    private val foregroundDispatcher: CoroutineDispatcher,
    private val errorHandle: ApiErrorHandle
) where Type : Any {

    private val job = Job()

    private val backgroundCoroutineContext: CoroutineContext
        get() = job + backgroundDispatcher
    private val backgroundScope = CoroutineScope(backgroundCoroutineContext)
    private val foregroundCoroutineContext: CoroutineContext
        get() = job + foregroundDispatcher
    private val foregroundScope = CoroutineScope(foregroundCoroutineContext)

    abstract suspend fun run(params: Params): Type

    fun execute(
        params: Params,
        onSuccess: (Type) -> Unit,
        onError: (ErrorModel) -> Unit
    ) {
        backgroundScope.launch {
            try{
                val response = run(params)
                foregroundScope.launch {
                    onSuccess(response)
                }
            } catch (e: Exception) {
                foregroundScope.launch {
                    onError(errorHandle.traceErrorException(e))
                }
            }
        }
    }

    fun unsubscribe() {
        backgroundCoroutineContext.cancel()
    }

}