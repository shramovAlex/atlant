package com.atlant.domain.usecase


import com.atlant.data.model.JwtToken
import com.atlant.domain.entity.ApiErrorHandle
import com.atlant.domain.params.LoginParams
import com.atlant.domain.usecase.common.BaseUseCase
import com.atlant.manager.SharedPreferencesManager
import com.atlant.manager.TokenManager
import com.atlant.network.ApiService
import kotlinx.coroutines.CoroutineDispatcher

/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
class LoginUseCase(
    backgroundDispatcher: CoroutineDispatcher,
    foregroundDispatcher: CoroutineDispatcher,
    private val apiService: ApiService,
    apiErrorHandle: ApiErrorHandle,
    private val sharedPreferencesManager: SharedPreferencesManager
) : BaseUseCase<JwtToken, LoginParams>(
    backgroundDispatcher,
    foregroundDispatcher,
    apiErrorHandle
) {

    override suspend fun run(params: LoginParams): JwtToken {
        try {
            val token = apiService.login(params).token ?: ""
            val account = JwtToken(token)
            TokenManager.jwtToken = account
            sharedPreferencesManager.putAccountToken(token)
            return account
        } catch (e: Exception) {
            throw Exception("Token is not valid")
        }
    }
}