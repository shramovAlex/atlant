package com.atlant.domain.response

import com.google.gson.annotations.SerializedName

/**
 * @author Alexandr Shramov
 * @date 05.12.2019
 */
data class LoginResponse(
    @SerializedName("token") val token: String? = null
)