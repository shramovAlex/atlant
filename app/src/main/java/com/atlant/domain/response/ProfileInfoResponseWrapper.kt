package com.atlant.domain.response

import com.atlant.data.model.ProfileInfo
import com.google.gson.annotations.SerializedName

/**
 * @author Alexandr Shramov
 * @date 07.12.2019
 */
data class ProfileInfoResponseWrapper(
    @SerializedName("info") val profileInfo: ProfileInfo? = null
)