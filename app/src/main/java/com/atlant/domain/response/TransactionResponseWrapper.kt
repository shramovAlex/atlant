package com.atlant.domain.response

import com.google.gson.annotations.SerializedName

/**
 * @author Alexandr Shramov
 * @date 08.12.2019
 */
data class TransactionResponseWrapper(
    @SerializedName("op") val op : String,
    @SerializedName("x") val element: TransactionResponseElement?
)