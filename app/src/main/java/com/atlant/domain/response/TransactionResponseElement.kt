package com.atlant.domain.response

import com.google.gson.annotations.SerializedName

/**
 * @author Alexandr Shramov
 * @date 08.12.2019
 */
data class TransactionResponseElement(
    @SerializedName("time") val time : Long,
    @SerializedName("hash") val hash : String?
)