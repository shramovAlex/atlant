package com.atlant.domain.repository

import com.atlant.data.model.ProfileInfo
import com.atlant.domain.response.ProfileInfoResponseWrapper

/**
 * @author Alexandr Shramov
 * @date 07.12.2019
 */
interface ProfileInfoRepository {
    suspend fun getProfileInfo(): ProfileInfo
    suspend fun getRemoteProfileInfo(): ProfileInfoResponseWrapper
}