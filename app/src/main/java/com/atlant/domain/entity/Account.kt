package com.atlant.domain.entity

import com.google.gson.annotations.SerializedName

/**
 * @author Alexandr Shramov
 * @date 07.12.2019
 */
data class Account(
    @SerializedName("account_id") val accountId : String? = null,
    @SerializedName("account_type") val accountType : String? = null,
    @SerializedName("email") val email : String? = null,
    @SerializedName("email_verified") val emailVerified : Boolean = false,
    @SerializedName("phone") val phone : String? = null,
    @SerializedName("totp_verified") val totpVerified : Boolean = false,
    @SerializedName("2fa_method") val twoFactorAuthMethod : String? = null,
    @SerializedName("password") val password : String,
    @SerializedName("created_at") val createdAt : String
)