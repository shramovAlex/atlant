package com.atlant.domain.entity

import com.google.gson.annotations.SerializedName

/**
 * @author Alexandr Shramov
 * @date 07.12.2019
 */
data class Profile(
    @SerializedName("profile_id") val profileId : String? = null,
    @SerializedName("account_id") val accountId : String? = null,
    @SerializedName("profile_type") val profileType : String? = null,
    @SerializedName("first_name") val firstName : String? = null,
    @SerializedName("last_name") val lastName : String? = null,
    @SerializedName("location") val location : String? = null,
    @SerializedName("gender") val gender : String? = null,
    @SerializedName("phone_country") val phoneCountry : String? = null,
    @SerializedName("phone_number") val phoneNumber : String? = null,
    @SerializedName("email") val email : String? = null,
    @SerializedName("avatar_url") val avatarUrl : String? = null,
    @SerializedName("kyc_verified") val kycVerified : Boolean,
    @SerializedName("langs_spoken_names") val langsSpokenNames : List<String?>? = null,
    @SerializedName("joined_at") val joinedAt : String? = null
)