package com.atlant.domain.entity

import android.content.Context
import com.atlant.R
import com.atlant.data.model.ErrorModel
import okhttp3.ResponseBody
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

/**
 * @author Alexandr Shramov
 * @date 04.12.19
 */
class ApiErrorHandle(private val mContext: Context) {

    fun traceErrorException(throwable: Throwable?): ErrorModel {
        return when (throwable) {
            is HttpException -> {
                if (throwable.code() == 401) {
                    ErrorModel(
                        throwable.message(),
                        throwable.code(),
                        ErrorModel.ErrorStatus.UNAUTHORIZED,
                        mContext.getString(R.string.error_unauthorized)
                    )
                } else {
                    getHttpError(throwable.response()?.errorBody())
                }
            }
            is SocketTimeoutException -> {
                ErrorModel(
                    throwable.message,
                    ErrorModel.ErrorStatus.TIMEOUT,
                    mContext.getString(R.string.error_timeout)
                )
            }
            is IOException -> {
                ErrorModel(
                    throwable.message,
                    ErrorModel.ErrorStatus.NO_CONNECTION,
                    mContext.getString(R.string.error_no_connection)
                )
            }
            else -> ErrorModel(
                throwable?.message,
                ErrorModel.ErrorStatus.NOT_DEFINED,
                mContext.getString(R.string.error_unknown)
            )
        }
    }

    private fun getHttpError(body: ResponseBody?): ErrorModel {
        return try {
            // use response body to get error detail
            ErrorModel(
                body.toString(),
                400,
                ErrorModel.ErrorStatus.BAD_RESPONSE,
                mContext.getString(R.string.error_bad_response)
            )
        } catch (e: Throwable) {
            e.printStackTrace()
            ErrorModel(
                e.message,
                ErrorModel.ErrorStatus.NOT_DEFINED,
                mContext.getString(R.string.error_unknown)
            )
        }
    }
}