package com.atlant.domain.params

import com.google.gson.annotations.SerializedName

/**
 * @author Alexandr Shramov
 * @date 08.12.2019
 */
data class LogoutParams(
    @SerializedName("session_id") val sessionId: String
)