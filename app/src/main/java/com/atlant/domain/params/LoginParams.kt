package com.atlant.domain.params

/**
 * @author Alexandr Shramov
 * @date 04.12.19
 */
data class LoginParams(
    val email: String,
    val password: String
)