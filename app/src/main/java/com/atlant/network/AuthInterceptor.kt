package com.atlant.network

import com.atlant.data.model.JwtToken
import com.atlant.domain.response.LoginResponse
import com.atlant.manager.TokenManager
import com.atlant.utils.TOKEN_REFRESH_URL
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response

/**
 * @author Alexandr Shramov
 * @date 05.12.2019
 */
class AuthInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val token = TokenManager.jwtToken ?: return chain.proceed(originalRequest)
        val tokenStr = token.token

        return if (token.isTokenValid() || !TokenManager.isAuthorized()) {
            chain.proceedDeletingTokenOnError(
                chain.request().newBuilder().addHeaders(tokenStr).build())
        } else {
            val refreshTokenRequest =
                originalRequest
                    .newBuilder()
                    .post(RequestBody.create(null, ""))
                    .url(TOKEN_REFRESH_URL)
                    .addHeaders(tokenStr)
                    .build()

            val refreshResponse = chain.proceedDeletingTokenOnError(refreshTokenRequest)
            if (refreshResponse.isSuccessful) {
                val refreshedToken = Gson().fromJson(refreshResponse.body()?.string(),
                    LoginResponse::class.java).token ?: ""
                try {
                    TokenManager.jwtToken = JwtToken(refreshedToken)
                } catch (e: Exception) {
                    // invalid token, just proceed original request
                    chain.proceedDeletingTokenOnError(chain.request())
                }
                val newCall = originalRequest.newBuilder().addHeaders(refreshedToken).build()
                chain.proceedDeletingTokenOnError(newCall)
            } else {
                chain.proceedDeletingTokenOnError(chain.request())
            }
        }
    }
}

private fun Interceptor.Chain.proceedDeletingTokenOnError(request: Request): Response {
    val response = proceed(request)
    if (response.code() == 401) {
        TokenManager.deleteToken()
    }
    return response
}

private fun Request.Builder.addHeaders(token: String) =
    this.apply { header("Authorization", token) }
