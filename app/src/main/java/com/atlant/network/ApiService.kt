package com.atlant.network


import com.atlant.domain.params.LoginParams
import com.atlant.domain.params.LogoutParams
import com.atlant.domain.response.LoginResponse
import com.atlant.domain.response.ProfileInfoResponseWrapper
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
interface ApiService {

    @POST("accounts/auth")
    suspend fun login(@Body params: LoginParams): LoginResponse

    @GET("accounts/current")
    suspend fun getProfileInfo() : ProfileInfoResponseWrapper

    @POST ("accounts/sessions/end")
    suspend fun logout(@Body params: LogoutParams): Unit
}