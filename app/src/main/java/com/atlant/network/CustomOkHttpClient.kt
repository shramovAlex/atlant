package com.atlant.network


import android.content.Context
import com.atlant.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
class CustomOkHttpClient : OkHttpClient() {

    fun getOkHttpClient(context: Context): OkHttpClient {

        val okHttpClientBuilder = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(AuthInterceptor())

        if (BuildConfig.DEBUG) {
            okHttpClientBuilder.addInterceptor(setLogging())
        }

        return okHttpClientBuilder.build()
    }

    private fun setLogging(): Interceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    companion object {
        const val CONNECT_TIMEOUT = 60L // 60 seconds
        const val READ_WRITE_TIMEOUT = 120L // 120 seconds
    }

}