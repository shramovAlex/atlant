package com.atlant.app

import android.app.Application
import com.atlant.data.model.JwtToken
import com.atlant.di.AppContainer
import com.atlant.manager.TokenManager


/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
class AtlantApp : Application() {

    lateinit var appContainer: AppContainer

    override fun onCreate() {
        super.onCreate()
        appContainer = AppContainer(this.applicationContext)
        autoLogin(appContainer.provideSharedPreferences().getAccountToken())
    }

    private fun autoLogin(token: String?) {
        if(token != null) {
            TokenManager.jwtToken = JwtToken(token)
        }
    }



}