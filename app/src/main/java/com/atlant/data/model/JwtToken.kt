package com.atlant.data.model

import android.util.Base64
import org.json.JSONObject
import java.util.*

/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
data class JwtToken(
    var expired: Long = 0L,
    var accountId: String = "",
    var sessionId: String = "",
    var accountType: String = "",
    var token: String = ""
) {

    constructor(jwtToken: String): this() {
        val split =
            jwtToken.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val decoded = String(Base64.decode(split[1], Base64.URL_SAFE))
        val jsonObject = JSONObject(decoded)
        expired = jsonObject.get("exp").toString().toLong()
        accountId = jsonObject.get("account_id").toString()
        sessionId = jsonObject.get("session_id").toString()
        accountType = jsonObject.get("account_type").toString()
        this.token = jwtToken
    }

    fun isTokenValid(): Boolean {
        val currentTime = (Calendar.getInstance(TimeZone.getTimeZone("UTC")).timeInMillis / 1000)
        val isValid = currentTime <= expired
        return isValid
    }

}