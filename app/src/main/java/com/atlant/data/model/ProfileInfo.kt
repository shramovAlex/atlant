package com.atlant.data.model

import com.atlant.domain.entity.Account
import com.atlant.domain.entity.Profile

/**
 * @author Alexandr Shramov
 * @date 07.12.2019
 */
data class ProfileInfo(
    val account : Account?,
    val profiles : List<Profile>?
)