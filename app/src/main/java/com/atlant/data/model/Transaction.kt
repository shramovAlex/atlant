package com.atlant.data.model

import com.atlant.data.model.common.DisplayableItem

/**
 * @author Alexandr Shramov
 * @date 08.12.2019
 */
data class Transaction(
    val hash: String = "",
    val time: String = ""
) : DisplayableItem