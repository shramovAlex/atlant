package com.atlant.data.model

/**
 * @author Alexandr Shramov
 * @date 04.12.19
 */
data class ErrorModel(
    val originalMessage: String? = null,
    val code: Int? = null,
    var errorStatus: ErrorStatus,
    var errorMessage: String
) {

    constructor(message: String?,
                errorStatus: ErrorStatus,
                errorMessage: String
    ) : this(message, null, errorStatus, errorMessage)

    enum class ErrorStatus {
        NO_CONNECTION,
        BAD_RESPONSE,
        TIMEOUT,
        EMPTY_RESPONSE,
        NOT_DEFINED,
        UNAUTHORIZED
    }
}