package com.atlant.data.repository

import com.atlant.data.model.ProfileInfo
import com.atlant.domain.repository.ProfileInfoRepository
import com.atlant.domain.response.ProfileInfoResponseWrapper
import com.atlant.network.ApiService

/**
 * @author Alexandr Shramov
 * @date 07.12.2019
 */
class ProfileInfoRepositoryImpl(
    private val mApiService: ApiService
) : ProfileInfoRepository {

    override suspend fun getProfileInfo(): ProfileInfo {
        val response = getRemoteProfileInfo().profileInfo
        return ProfileInfo(response?.account, response?.profiles)
    }

    override suspend fun getRemoteProfileInfo(): ProfileInfoResponseWrapper {
        return mApiService.getProfileInfo()
    }
}