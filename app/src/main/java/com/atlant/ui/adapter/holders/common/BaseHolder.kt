package com.atlant.ui.adapter.holders.common

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.atlant.data.model.common.DisplayableItem

/**
 * @author Alexandr Shramov
 * @date 08.12.2019
 */
abstract class BaseHolder<ItemType: DisplayableItem>(v: View) : RecyclerView.ViewHolder(v) {

    protected lateinit var mItem: ItemType

    fun onBind(item: ItemType) {
        mItem = item
        bind()
    }

    protected abstract fun bind()

}