package com.atlant.ui.adapter.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.atlant.data.model.common.DisplayableItem
import com.atlant.ui.adapter.holders.common.BaseHolder

/**
 * @author Alexandr Shramov
 * @date 08.12.2019
 */
abstract class BaseAdapter<ViewHolderType: BaseHolder<DataType>, DataType: DisplayableItem> (
    private val mLayoutResId: Int
) : RecyclerView.Adapter<ViewHolderType>() {

    private val mData: ArrayList<DataType> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderType {
        val view = LayoutInflater.from(parent.context).inflate(mLayoutResId, parent, false)
        return createViewHolder(view)
    }

    abstract fun createViewHolder(view: View): ViewHolderType

    override fun getItemCount() = mData.size

    override fun onBindViewHolder(holder: ViewHolderType, position: Int) {
        if(itemCount > 0) {
            holder.onBind(mData[position])
        }
    }

    fun insertAtStart(item: DataType) {
        mData.add(0, item)
        notifyItemInserted(0)
    }

    fun clearData() {
        mData.clear()
        notifyDataSetChanged()
    }
}