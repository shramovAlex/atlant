package com.atlant.ui.adapter.holders

import android.view.View
import android.widget.TextView
import com.atlant.R
import com.atlant.data.model.Transaction
import com.atlant.ui.adapter.holders.common.BaseHolder
import kotlinx.android.synthetic.main.item_transaction.view.*

/**
 * @author Alexandr Shramov
 * @date 08.12.2019
 */
class TransactionHolder(v: View) : BaseHolder<Transaction>(v) {

    private val mHashTv: TextView = v.item_transaction_hash_tv
    private val mTimeTv: TextView = v.item_transaction_date_tv

    override fun bind() {
        mHashTv.text = mHashTv.context.getString(R.string.transaction_hash, mItem.hash)
        mTimeTv.text = mTimeTv.context.getString(R.string.transaction_time, mItem.time)
    }
}
