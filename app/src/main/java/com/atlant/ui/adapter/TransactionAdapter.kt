package com.atlant.ui.adapter

import android.view.View
import com.atlant.R
import com.atlant.data.model.Transaction
import com.atlant.ui.adapter.common.BaseAdapter
import com.atlant.ui.adapter.holders.TransactionHolder

/**
 * @author Alexandr Shramov
 * @date 08.12.2019
 */
class TransactionAdapter : BaseAdapter<TransactionHolder, Transaction>(
    R.layout.item_transaction
) {
    override fun createViewHolder(view: View): TransactionHolder {
        return TransactionHolder(view)
    }
}