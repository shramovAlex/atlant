package com.atlant.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.atlant.R
import com.atlant.app.AtlantApp
import com.atlant.manager.TokenManager
import com.atlant.manager.common.Router

class MainActivity : AppCompatActivity() {

    private val mRouter: Router by lazy {
        (application as AtlantApp).appContainer.provideRouter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(TokenManager.isAuthorized()) {
            mRouter.transferToAccountProfileFragment(this)
        } else {
            mRouter.transferToAuthFragment(this)
        }
    }
}
