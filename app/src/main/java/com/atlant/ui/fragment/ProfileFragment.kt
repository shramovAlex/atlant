package com.atlant.ui.fragment

import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.atlant.R
import com.atlant.app.AtlantApp
import com.atlant.data.model.Transaction
import com.atlant.manager.common.Router
import com.atlant.mvp.presenter.fragment.ProfileFragmentPresenter
import com.atlant.mvp.view.fragment.ProfileFragmentView
import com.atlant.ui.adapter.TransactionAdapter
import com.atlant.ui.fragment.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_profile.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter

/**
 * @author Alexandr Shramov
 * @date 05.12.2019
 */
class ProfileFragment : BaseFragment(), ProfileFragmentView  {

    @InjectPresenter internal lateinit var mPresenter: ProfileFragmentPresenter
    private val mRouter: Router by lazy {
        (activity?.application as AtlantApp).appContainer.provideRouter()
    }
    private val mAdapter: TransactionAdapter = TransactionAdapter()

    override fun getLayout() = R.layout.fragment_profile

    @ProvidePresenter
    fun providePresenter(): ProfileFragmentPresenter {
        val container = (activity?.application as AtlantApp).appContainer
        return ProfileFragmentPresenter(
            container.provideGetProfileInfoUseCase(),
            container.provideWebSocketManager(),
            container.provideLogoutUseCase()
        )
    }

    override fun initView() {
        fragment_profile_transactions_rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        fragment_profile_transactions_rv.adapter = mAdapter

        fragment_profile_connect.setOnClickListener {
            fragment_profile_connect.isEnabled = false
            fragment_profile_disconnect.isEnabled = true
            mPresenter.connectToBlockchainTransactions()
        }
        fragment_profile_clear.setOnClickListener {
            fragment_profile_transactions_count_tv.text = getString(R.string.total_transactions, 0)
            mAdapter.clearData()
        }
        fragment_profile_disconnect.setOnClickListener {
            fragment_profile_connect.isEnabled = true
            fragment_profile_disconnect.isEnabled = false
            mPresenter.disconnectFromBlockchainTransaction()
        }

        fragment_profile_logout.setOnClickListener {
            mPresenter.logout()
        }
    }

    override fun setProfileFirstName(firstName: String) {
        if(firstName.isNotEmpty()) {
            fragment_profile_first_name_tv.text = getString(R.string.profile_info_first_name, firstName)
            fragment_profile_first_name_tv.visibility = View.VISIBLE
        } else {
            fragment_profile_first_name_tv.visibility = View.GONE
        }
    }

    override fun setProfileLastName(lastName: String) {
        if(lastName.isNotEmpty()) {
            fragment_profile_last_name_tv.text = getString(R.string.profile_info_last_name, lastName)
            fragment_profile_last_name_tv.visibility = View.VISIBLE
        } else {
            fragment_profile_last_name_tv.visibility = View.GONE
        }
    }

    override fun setProfileType(type: String) {
        if(type.isNotEmpty()) {
            fragment_profile_type_tv.text = getString(R.string.profile_info_type, type)
            fragment_profile_type_tv.visibility = View.VISIBLE
        } else {
            fragment_profile_type_tv.visibility = View.GONE
        }
    }

    override fun setProfileEmail(email: String) {
        if(email.isNotEmpty()) {
            fragment_profile_email_tv.text = getString(R.string.profile_info_email, email)
            fragment_profile_email_tv.visibility = View.VISIBLE
        } else {
            fragment_profile_email_tv.visibility = View.GONE
        }
    }

    override fun setProfileRegDate(regDate: String) {
        if(regDate.isNotEmpty()) {
            fragment_profile_reg_date_tv.text = getString(R.string.profile_info_reg_date, regDate)
            fragment_profile_reg_date_tv.visibility = View.VISIBLE
        } else {
            fragment_profile_reg_date_tv.visibility = View.GONE
        }
    }

    override fun hideProgressBar() {
        fragment_profile_progress_bar.visibility = View.GONE
    }

    override fun showError(errorText: String?) {
        fragment_profile_first_name_tv.visibility = View.GONE
        fragment_profile_last_name_tv.visibility = View.GONE
        fragment_profile_type_tv.visibility = View.GONE
        fragment_profile_email_tv.visibility = View.GONE
        fragment_profile_reg_date_tv.visibility = View.GONE
        fragment_profile_progress_bar.visibility = View.GONE
        fragment_profile_error_tv.text = errorText
        fragment_profile_error_tv.visibility = View.VISIBLE
    }

    override fun showUnauthorized() {
        Toast.makeText(context, getString(R.string.error_unauthorized), Toast.LENGTH_SHORT).show()
    }

    override fun transferToAuthorizationFragment() {
        mRouter.transferToAuthFragment(activity)
    }

    override fun insertData(transaction: Transaction) {
        mAdapter.insertAtStart(transaction)
        fragment_profile_transactions_count_tv.text =
            getString(R.string.total_transactions, mAdapter.itemCount)
        fragment_profile_transactions_rv.smoothScrollToPosition(0)
    }

    companion object {
        const val TAG = "ProfileFragment"
    }
}