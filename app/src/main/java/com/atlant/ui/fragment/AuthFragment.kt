package com.atlant.ui.fragment

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.atlant.BuildConfig
import com.atlant.R
import com.atlant.app.AtlantApp
import com.atlant.manager.common.Router
import com.atlant.mvp.presenter.fragment.AuthFragmentPresenter
import com.atlant.mvp.view.fragment.AuthFragmentView
import com.atlant.ui.fragment.common.BaseFragment
import kotlinx.android.synthetic.main.fragment_auth.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter


/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
class AuthFragment : BaseFragment(), AuthFragmentView {

    @InjectPresenter internal lateinit var mPresenter: AuthFragmentPresenter

    private val mRouter: Router by lazy {
        (activity?.application as AtlantApp).appContainer.provideRouter()
    }

    override fun getLayout(): Int = R.layout.fragment_auth

    @ProvidePresenter
    fun providePresenter(): AuthFragmentPresenter {
        return AuthFragmentPresenter(
            (activity?.application as AtlantApp).appContainer.provideLoginUseCase()
        )
    }

    override fun initView() {
        fragment_auth_email_et.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mPresenter.onLoginTextChanged(p0.toString())
            }
        })
        fragment_auth_password_et.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mPresenter.onPasswordChanged(p0.toString())
            }
        })
        if(BuildConfig.DEBUG) {
            fragment_auth_email_et.setText("hello@karta.com")
            fragment_auth_password_et.setText("12345678")
        }
        fragment_auth_btn.setOnClickListener { mPresenter?.onAuthButtonClicked() }
    }

    override fun showLoginEmptyMsg() {
        Toast.makeText(context, getString(R.string.email_empty_msg), Toast.LENGTH_SHORT).show()
    }

    override fun showPasswordEmptyMsg() {
        Toast.makeText(context, getString(R.string.password_empty_msg), Toast.LENGTH_SHORT).show()
    }

    override fun showProgressBar() {
        fragment_auth_btn.visibility = View.GONE
        fragment_progress_bar.visibility = View.VISIBLE
    }

    private fun showAuthBtn() {
        fragment_auth_btn.visibility = View.VISIBLE
        fragment_progress_bar.visibility = View.GONE
    }

    override fun showError(e: String?) {
        Toast.makeText(context, "error: $e", Toast.LENGTH_SHORT).show()
        showAuthBtn()
    }

    override fun transferToAccountProfile() {
        mRouter.transferToAccountProfileFragment(activity)
    }

    companion object {
        const val TAG = "AuthFragment"
    }
}