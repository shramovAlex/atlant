package com.atlant.di

import android.content.Context
import com.atlant.data.repository.ProfileInfoRepositoryImpl
import com.atlant.domain.entity.ApiErrorHandle
import com.atlant.domain.repository.ProfileInfoRepository
import com.atlant.domain.usecase.GetProfileInfoUseCase
import com.atlant.domain.usecase.LoginUseCase
import com.atlant.domain.usecase.LogoutUseCase
import com.atlant.manager.RouterImpl
import com.atlant.manager.SharedPreferencesManager
import com.atlant.manager.WebSocketTransactionsManager
import com.atlant.manager.common.Router
import com.atlant.network.ApiService
import com.atlant.network.CustomOkHttpClient
import com.atlant.utils.BASE_URL
import com.atlant.utils.BLOCKCHAIN_URL
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
class AppContainer(private val mContext: Context) {

    private val mRouter: Router by lazy { RouterImpl() }
    private val mSharedPreferencesManager = SharedPreferencesManager(
        mContext.getSharedPreferences(
            SharedPreferencesManager.SHARED_PREF_FILE_NAME,
            Context.MODE_PRIVATE))
    private val mHttpClient: OkHttpClient by lazy {
        CustomOkHttpClient().getOkHttpClient(mContext)
    }
    private val mWebSocketHttpClient: OkHttpClient by lazy {
        OkHttpClient.Builder().build()
    }
    private val mApiService: ApiService by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(provideHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }
    private val mWebSocketRequest: Request by lazy {
        Request.Builder().url(BLOCKCHAIN_URL).build()
    }
    private val mApiErrorHandle: ApiErrorHandle by lazy {
        ApiErrorHandle(provideApplicationContext())
    }

    private val mBackgroundDispatcher: CoroutineDispatcher = Dispatchers.IO
    private val mForegroundDispatcher: CoroutineDispatcher = Dispatchers.Main
    private val mProfileInfoRepository: ProfileInfoRepository by lazy {
        ProfileInfoRepositoryImpl(provideApi())
    }
    private val mWebSocketTransactionsManager: WebSocketTransactionsManager by lazy {
        WebSocketTransactionsManager(
            provideWebSockerHttpClient(),
            provideWebSocketRequest(),
            provideForegroundDispatcher()
        )
    }

    fun provideApplicationContext() = mContext

    fun provideBackgroundDispatcher() = mBackgroundDispatcher

    fun provideForegroundDispatcher() = mForegroundDispatcher

    fun provideRouter() = mRouter

    fun provideSharedPreferences() = mSharedPreferencesManager

    fun provideApi() = mApiService

    fun provideApiErrorHandle() = mApiErrorHandle

    fun provideHttpClient() = mHttpClient

    fun provideWebSockerHttpClient() = mWebSocketHttpClient

    fun provideLoginUseCase() = LoginUseCase(
        provideBackgroundDispatcher(),
        provideForegroundDispatcher(),
        provideApi(),
        provideApiErrorHandle(),
        provideSharedPreferences())

    fun provideGetProfileInfoUseCase() = GetProfileInfoUseCase(
        provideBackgroundDispatcher(),
        provideForegroundDispatcher(),
        provideProfileInfoRepository(),
        provideApiErrorHandle())

    fun provideProfileInfoRepository() = mProfileInfoRepository

    fun provideWebSocketRequest() = mWebSocketRequest

    fun provideWebSocketManager() = mWebSocketTransactionsManager

    fun provideLogoutUseCase() = LogoutUseCase(provideBackgroundDispatcher(),
        provideForegroundDispatcher(),
        provideApi(),
        provideApiErrorHandle(),
        provideSharedPreferences())

}