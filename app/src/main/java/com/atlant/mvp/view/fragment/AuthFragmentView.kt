package com.atlant.mvp.view.fragment

import com.atlant.mvp.view.fragment.base.BaseFragmentView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
interface AuthFragmentView : BaseFragmentView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun initView()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoginEmptyMsg()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showPasswordEmptyMsg()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showProgressBar()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showError(e: String?)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun transferToAccountProfile()

}