package com.atlant.mvp.view.fragment.base

import moxy.MvpView

/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
interface BaseFragmentView : MvpView {
}