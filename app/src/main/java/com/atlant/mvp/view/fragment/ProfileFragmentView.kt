package com.atlant.mvp.view.fragment

import com.atlant.data.model.Transaction
import com.atlant.mvp.view.fragment.base.BaseFragmentView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

/**
 * @author Alexandr Shramov
 * @date 05.12.2019
 */
interface ProfileFragmentView : BaseFragmentView {

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun initView()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setProfileFirstName(firstName: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setProfileLastName(lastName: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setProfileType(type: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setProfileEmail(email: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setProfileRegDate(regDate: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun hideProgressBar()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showError(errorText: String?)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showUnauthorized()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun transferToAuthorizationFragment()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun insertData(transaction: Transaction)

}