package com.atlant.mvp.presenter.fragment.base

import com.atlant.mvp.view.fragment.base.BaseFragmentView
import moxy.MvpPresenter

/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
abstract class BaseFragmentPresenter<ViewType : BaseFragmentView> : MvpPresenter<ViewType>() {

    override fun onDestroy() {
        super.onDestroy()
        release()
    }

    open fun release() {
        // for further implementation
    }

}