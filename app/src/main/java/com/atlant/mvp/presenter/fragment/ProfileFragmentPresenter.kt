package com.atlant.mvp.presenter.fragment

import com.atlant.data.model.ProfileInfo
import com.atlant.data.model.Transaction
import com.atlant.domain.params.LogoutParams
import com.atlant.domain.usecase.GetProfileInfoUseCase
import com.atlant.domain.usecase.LogoutUseCase
import com.atlant.manager.TokenManager
import com.atlant.manager.WebSocketTransactionsManager
import com.atlant.mvp.presenter.fragment.base.BaseFragmentPresenter
import com.atlant.mvp.view.fragment.ProfileFragmentView
import com.atlant.utils.API_DATE_FORMAT
import com.atlant.utils.APP_DATE_FORMAT
import moxy.InjectViewState
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author Alexandr Shramov
 * @date 05.12.2019
 */
@InjectViewState
class ProfileFragmentPresenter(
    private val mGetProfileInfoUseCase: GetProfileInfoUseCase,
    private val mWebSocketTransactionsManager: WebSocketTransactionsManager,
    private val mLogoutUseCase: LogoutUseCase
) : BaseFragmentPresenter<ProfileFragmentView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.initView()
        getProfileInfo()
        initWebSocketTransactionsManager()
    }

    override fun release() {
        super.release()
        mGetProfileInfoUseCase.unsubscribe()
        mWebSocketTransactionsManager.disconnect()
    }

    private fun getProfileInfo() {
        mGetProfileInfoUseCase.execute(
            Unit,
            onSuccess = { handleSuccessGetProfileInfo(it) },
            onError = {
                if(it.code == 401) {
                    viewState.showUnauthorized()
                } else {
                    viewState.showError(it.errorMessage)
                }
            })
    }

    private fun handleSuccessGetProfileInfo(profileInfo: ProfileInfo) {
        val profiles = profileInfo.profiles ?: listOf()
        if(profiles.isNotEmpty()) {
            val profile = profiles[0]
            viewState.setProfileFirstName(profile.firstName ?: "")
            viewState.setProfileLastName(profile.lastName ?: "")
            viewState.setProfileEmail(profile.email ?: "")
            viewState.setProfileType(profile.profileType ?: "")

            val regDate =
                try {
                    SimpleDateFormat(API_DATE_FORMAT, Locale.getDefault())
                        .parse(profile.joinedAt ?: "")?.time ?: 0L
                } catch (e: Exception) {
                    0L
                }
            if(regDate > 0L) {
                val formattedDate =
                    SimpleDateFormat(APP_DATE_FORMAT, Locale.getDefault())
                        .format(regDate)
                viewState.setProfileRegDate(formattedDate)
            } else {
                viewState.setProfileRegDate("")
            }
            viewState.hideProgressBar()
        }
    }

    private fun initWebSocketTransactionsManager() {
        mWebSocketTransactionsManager.onMessageReceived = { onMessageReceived(it) }
    }

    private fun onMessageReceived(transaction: Transaction) {
        viewState.insertData(transaction)
    }

    fun connectToBlockchainTransactions() {
        mWebSocketTransactionsManager.init()
    }

    fun disconnectFromBlockchainTransaction() {
        mWebSocketTransactionsManager.disconnect()
    }

    fun logout() {
        val sessionId = TokenManager.jwtToken?.sessionId ?: ""
        mLogoutUseCase.execute(LogoutParams(sessionId),
            onSuccess = {
                viewState.transferToAuthorizationFragment()
            },
            onError = {
                viewState.showError(it.errorMessage)
            }
        )
    }
}