package com.atlant.mvp.presenter.fragment

import android.util.Log
import com.atlant.domain.params.LoginParams
import com.atlant.domain.usecase.LoginUseCase
import com.atlant.mvp.presenter.fragment.base.BaseFragmentPresenter
import com.atlant.mvp.view.fragment.AuthFragmentView
import moxy.InjectViewState

/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
@InjectViewState
class AuthFragmentPresenter(
    private val mLoginUseCase: LoginUseCase
) : BaseFragmentPresenter<AuthFragmentView>() {

    private var mEmail: String = ""
    private var mPassword: String = ""

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.initView()
    }

    fun onLoginTextChanged(text: String) {
        mEmail = text
    }

    fun onPasswordChanged(text: String) {
        mPassword = text
    }

    fun onAuthButtonClicked() {
        if(mEmail.isNotEmpty()) {
            if(mPassword.isNotEmpty()) {
                login(mEmail, mPassword)
            } else {
                viewState.showPasswordEmptyMsg()
            }
        } else {
            viewState.showLoginEmptyMsg()
        }
    }

    private fun login(email: String, password: String) {
        viewState.showProgressBar()

        mLoginUseCase.execute(LoginParams(email, password),
            onSuccess = {
                viewState.transferToAccountProfile()
            },
            onError = {
                viewState.showError(it.errorMessage)
                Log.i("AtlantRequest", "error")
        })
    }
}