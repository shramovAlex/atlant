package com.atlant.utils

/**
 * @author Alexandr Shramov
 * @date 03.12.2019
 */
const val BASE_URL = "https://api.dev.karta.com/"
const val BLOCKCHAIN_URL = "wss://ws.blockchain.info/inv"
const val TOKEN_REFRESH_URL = "https://api.dev.karta.com/accounts/sessions/refresh"
const val APP_TAG = "Atlant"
const val API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
const val APP_DATE_FORMAT = "yyyy-MM-dd"
const val TRANSACTION_TIME_FORMAT = "HH:mm:ss"